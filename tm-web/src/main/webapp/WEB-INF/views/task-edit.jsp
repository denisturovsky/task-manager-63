<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../include/_header.jsp"/>

<h3>TASK EDIT</h3>

<form action="/task/edit/?id=${task.id}" method="POST">
    <input type="hidden" name="id" value="${task.id}"/>
    <p>
    <div>Name:</div>
    <div><input type="text" name="name" value="${task.name}"/></div>
    </p>
    <p>
    <div>Description:</div>
    <div><input type="text" name="description" value="${task.description}"/></div>
    </p>
    <p>
    <div>Status:</div>
    <select name="status">
        <c:forEach var="status" items="${statuses}">
            <option
                    <c:if test="${task.status == status}">selected="selected"</c:if>
                    value="${status}">${status.displayName}</option>
        </c:forEach>
    </select>
    </p>
    <p>
    <div>Created:</div>
    <div><input type="date" name="created" value="<fmt:formatDate pattern="yyyy-MM-dd" value="${task.created}"/>"/>
    </div>
    </p>
    <div>Project:</div>
    <select name="projectId">
        <option>-----</option>
        <c:forEach var="project" items="${projects}">
            <option
                    <c:if test="${project.id == task.projectId}">selected="selected"</c:if>
                    value="${project.id}">${project.name}</option>
        </c:forEach>
    </select>
    </p>
    <button type="submit" style="padding-top: 20px;">SAVE TASK</button>
</form>

<jsp:include page="../include/_footer.jsp"/>