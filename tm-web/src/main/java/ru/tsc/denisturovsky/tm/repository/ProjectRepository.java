package ru.tsc.denisturovsky.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.model.Project;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class ProjectRepository {

    private static final ProjectRepository INSTANCE = new ProjectRepository();

    @NotNull
    private Map<String, Project> projects = new LinkedHashMap<>();

    {
        add(new Project("First Project"));
        add(new Project("Second Project"));
        add(new Project("Third Project"));
    }

    public static ProjectRepository getInstance() {
        return INSTANCE;
    }

    public void add(@NotNull final Project project) {
        projects.put(project.getId(), project);
    }

    public void create() {
        add(new Project("New Project" + System.currentTimeMillis()));
    }

    @NotNull
    public Collection<Project> findAll() {
        return projects.values();
    }

    @Nullable
    public Project findById(@NotNull final String id) {
        return projects.get(id);
    }

    public void removeById(@NotNull final String id) {
        projects.remove(id);
    }

    public void save(@NotNull final Project project) {
        projects.put(project.getId(), project);
    }

}
