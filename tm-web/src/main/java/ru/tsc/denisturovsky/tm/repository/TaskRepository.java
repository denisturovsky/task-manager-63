package ru.tsc.denisturovsky.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.model.Task;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class TaskRepository {

    private static final TaskRepository INSTANCE = new TaskRepository();

    @NotNull
    private Map<String, Task> tasks = new LinkedHashMap<>();

    {
        add(new Task("First Task"));
        add(new Task("Second Task"));
        add(new Task("Third Task"));
    }

    public static TaskRepository getInstance() {
        return INSTANCE;
    }

    public void add(@NotNull final Task task) {
        tasks.put(task.getId(), task);
    }

    public void create() {
        add(new Task("New Task" + System.currentTimeMillis()));
    }

    @NotNull
    public Collection<Task> findAll() {
        return tasks.values();
    }

    @Nullable
    public Task findById(@NotNull final String id) {
        return tasks.get(id);
    }

    public void removeById(@NotNull final String id) {
        tasks.remove(id);
    }

    public void save(@NotNull final Task task) {
        tasks.put(task.getId(), task);
    }

}
