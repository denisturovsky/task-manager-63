package ru.tsc.denisturovsky.tm.servlet;

import ru.tsc.denisturovsky.tm.repository.ProjectRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/project/create/*")
public class ProjectCreateServlet extends HttpServlet {

    @Override
    protected void doGet(
            HttpServletRequest req,
            HttpServletResponse resp
    ) throws ServletException, IOException {
        ProjectRepository.getInstance().create();
        resp.sendRedirect("/projects");
    }

}