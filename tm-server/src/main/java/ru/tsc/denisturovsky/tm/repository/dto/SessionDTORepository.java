package ru.tsc.denisturovsky.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.denisturovsky.tm.dto.model.SessionDTO;

import java.util.List;
import java.util.Optional;

@Repository
@Scope("prototype")
public interface SessionDTORepository extends AbstractUserOwnedDTORepository<SessionDTO> {

    long countByUserId(@NotNull final String userId);

    @Transactional
    void deleteByUserId(@NotNull final String userId);

    @Transactional
    void deleteByUserIdAndId(
            @NotNull final String userId,
            @NotNull final String id
    );

    @Query("select case when count(c)> 0 then true else false end from SessionDTO c where userId = :userId and id = :id")
    boolean existByUserIdAndId(
            @Param("userId") String userId,
            @Param("id") String id
    );

    @NotNull
    List<SessionDTO> findByUserId(@NotNull final String userId);

    @NotNull
    List<SessionDTO> findByUserId(
            @NotNull final String userId,
            @NotNull final Sort sort
    );

    @NotNull
    Optional<SessionDTO> findByUserIdAndId(
            @NotNull final String userId,
            @NotNull final String id
    );

}