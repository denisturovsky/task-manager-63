package ru.tsc.denisturovsky.tm.sevice;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.tsc.denisturovsky.tm.api.service.IServerPropertyService;

@Getter
@Service
@PropertySource("classpath:application.properties")
public class ServerPropertyService implements IServerPropertyService {

    @Value("${buildNumber}")
    private String applicationVersion;

    @Value("${author.name}")
    private String authorName;

    @Value("${author.email}")
    private String authorEmail;

    @Value("${password.iteration}")
    private Integer passwordIteration;

    @Value("${password.secret}")
    private String passwordSecret;

    @Value("${server.port}")
    private String serverPort;

    @Value("${server.host}")
    private String serverHost;

    @Value("${jms.port}")
    private String JMSServerPort;

    @Value("${jms.host}")
    private String JMSServerHost;

    @Value("${session.key}")
    private String sessionKey;

    @Value("${session.timeout}")
    private Integer sessionTimeout;

    @Value("${database.username}")
    private String dBUser;

    @Value("${database.password}")
    private String dBPassword;

    @Value("${database.url}")
    private String dBUrl;

    @Value("${database.driver}")
    private String dBDriver;

    @Value("${database.dialect}")
    private String dBDialect;

    @Value("${database.show_sql}")
    private String dBShowSql;

    @Value("${database.hbm2ddl_auto}")
    private String dBHbm2ddlAuto;

    @Value("${database.format_sql}")
    private String dBFormatSql;

    @Value("${database.second_lvl_cache}")
    private String dBSecondLvlCache;

    @Value("${database.factory_class}")
    private String dBFactoryClass;

    @Value("${database.use_query_cache}")
    private String dBUseQueryCache;

    @Value("${database.use_min_puts}")
    private String dBUseMinPuts;

    @Value("${database.region_prefix}")
    private String dBRegionPrefix;

    @Value("${database.config_file_path}")
    private String dBConfigFilePath;

}