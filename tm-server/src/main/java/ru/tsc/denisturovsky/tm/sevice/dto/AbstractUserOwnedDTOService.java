package ru.tsc.denisturovsky.tm.sevice.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.denisturovsky.tm.api.service.dto.IUserOwnedDTOService;
import ru.tsc.denisturovsky.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.tsc.denisturovsky.tm.exception.field.UserIdEmptyException;
import ru.tsc.denisturovsky.tm.repository.dto.AbstractUserOwnedDTORepository;

import javax.persistence.EntityNotFoundException;

@Service
@NoArgsConstructor
public abstract class AbstractUserOwnedDTOService<M extends AbstractUserOwnedModelDTO>
        extends AbstractDTOService<M> implements IUserOwnedDTOService<M> {

    @NotNull
    @Override
    @Transactional
    public M add(
            @Nullable final String userId,
            @NotNull final M model
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        model.setUserId(userId);
        return getRepository().save(model);
    }

    @Nullable
    protected abstract AbstractUserOwnedDTORepository<M> getRepository();

    @Override
    @Transactional
    public void remove(
            @Nullable final String userId,
            @Nullable final M model
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null || findOneById(model.getUserId(), model.getId()) == null) throw new EntityNotFoundException();
        getRepository().delete(model);
    }

    @Override
    @Transactional
    public void update(
            @Nullable final String userId,
            @Nullable final M model
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null || findOneById(model.getUserId(), model.getId()) == null) throw new EntityNotFoundException();
        model.setUserId(userId);
        getRepository().save(model);
    }

}