package ru.tsc.denisturovsky.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.tsc.denisturovsky.tm.api.service.dto.IProjectDTOService;
import ru.tsc.denisturovsky.tm.api.service.dto.ITaskDTOService;
import ru.tsc.denisturovsky.tm.api.service.dto.IUserDTOService;
import ru.tsc.denisturovsky.tm.dto.model.TaskDTO;
import ru.tsc.denisturovsky.tm.dto.model.UserDTO;
import ru.tsc.denisturovsky.tm.enumerated.CustomSort;
import ru.tsc.denisturovsky.tm.enumerated.Status;
import ru.tsc.denisturovsky.tm.exception.entity.TaskNotFoundException;
import ru.tsc.denisturovsky.tm.exception.field.*;
import ru.tsc.denisturovsky.tm.marker.UnitCategory;

import javax.persistence.EntityNotFoundException;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static ru.tsc.denisturovsky.tm.constant.ContextTestData.CONTEXT;
import static ru.tsc.denisturovsky.tm.constant.ProjectTestData.USER_PROJECT1;
import static ru.tsc.denisturovsky.tm.constant.ProjectTestData.USER_PROJECT2;
import static ru.tsc.denisturovsky.tm.constant.TaskTestData.*;
import static ru.tsc.denisturovsky.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class TaskServiceTest {

    @NotNull
    private final static IUserDTOService USER_SERVICE = CONTEXT.getBean(IUserDTOService.class);

    @NotNull
    private final static ITaskDTOService SERVICE = CONTEXT.getBean(ITaskDTOService.class);

    @NotNull
    private final static IProjectDTOService PROJECT_SERVICE = CONTEXT.getBean(IProjectDTOService.class);

    @NotNull
    private static String USER_ID = "";

    @BeforeClass
    public static void setUp() throws Exception {
        @NotNull final UserDTO user = USER_SERVICE.create(USER_TEST_LOGIN, USER_TEST_PASSWORD);
        USER_ID = user.getId();
    }

    @AfterClass
    public static void tearDown() throws Exception {
        @Nullable final UserDTO user = USER_SERVICE.findByLogin(USER_TEST_LOGIN);
        if (user != null) USER_SERVICE.remove(user);
    }

    @Test
    public void addByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.add("", USER_TASK3));
        Assert.assertNotNull(SERVICE.add(USER_ID, USER_TASK3));
        @Nullable final TaskDTO task = SERVICE.findOneById(USER_ID, USER_TASK3.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK3.getId(), task.getId());
    }

    @After
    public void after() throws Exception {
        SERVICE.clear(USER_ID);
        PROJECT_SERVICE.clear(USER_ID);
    }

    @Before
    public void before() throws Exception {
        PROJECT_SERVICE.add(USER_ID, USER_PROJECT1);
        PROJECT_SERVICE.add(USER_ID, USER_PROJECT2);
        SERVICE.add(USER_ID, USER_TASK1);
        SERVICE.add(USER_ID, USER_TASK2);
    }

    @Test
    public void changeTaskStatusById() throws Exception {
        @NotNull final Status status = Status.COMPLETED;
        Assert.assertThrows(
                UserIdEmptyException.class, () -> SERVICE.changeTaskStatusById(null, USER_TASK1.getId(), status));
        Assert.assertThrows(
                UserIdEmptyException.class, () -> SERVICE.changeTaskStatusById("", USER_TASK1.getId(), status));
        Assert.assertThrows(IdEmptyException.class, () -> SERVICE.changeTaskStatusById(USER_ID, null, status));
        Assert.assertThrows(IdEmptyException.class, () -> SERVICE.changeTaskStatusById(USER_ID, "", status));
        Assert.assertThrows(
                StatusEmptyException.class, () -> SERVICE.changeTaskStatusById(USER_ID, USER_TASK1.getId(), null));
        Assert.assertThrows(
                TaskNotFoundException.class, () -> SERVICE.changeTaskStatusById(USER_ID, NON_EXISTING_TASK_ID, status));
        SERVICE.changeTaskStatusById(USER_ID, USER_TASK1.getId(), status);
        @Nullable final TaskDTO task = SERVICE.findOneById(USER_ID, USER_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(status, task.getStatus());
    }

    @Test
    public void clearByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.clear(""));
        SERVICE.clear(USER_ID);
        Assert.assertEquals(0, SERVICE.getSize(USER_ID));
    }

    @Test
    public void create() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.create(null, USER_TASK3.getName()));
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.create("", USER_TASK3.getName()));
        Assert.assertThrows(NameEmptyException.class, () -> SERVICE.create(USER_ID, null));
        Assert.assertThrows(NameEmptyException.class, () -> SERVICE.create(USER_ID, ""));
        @NotNull final TaskDTO task = SERVICE.create(USER_ID, USER_TASK3.getName());
        Assert.assertNotNull(task);
        @Nullable final TaskDTO findTask = SERVICE.findOneById(USER_ID, task.getId());
        Assert.assertNotNull(findTask);
        Assert.assertEquals(task.getId(), findTask.getId());
        Assert.assertEquals(USER_TASK3.getName(), task.getName());
        Assert.assertEquals(USER_ID, task.getUserId());
    }

    @Test
    public void createWithDescription() throws Exception {
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> SERVICE.create(null, USER_TASK3.getName(), USER_TASK3.getDescription())
        );
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> SERVICE.create("", USER_TASK3.getName(), USER_TASK3.getDescription())
        );
        Assert.assertThrows(NameEmptyException.class, () -> SERVICE.create(USER_ID, null, USER_TASK3.getDescription()));
        Assert.assertThrows(NameEmptyException.class, () -> SERVICE.create(USER_ID, "", USER_TASK3.getDescription()));
        Assert.assertThrows(DescriptionEmptyException.class, () -> SERVICE.create(USER_ID, USER_TASK3.getName(), null));
        Assert.assertThrows(DescriptionEmptyException.class, () -> SERVICE.create(USER_ID, USER_TASK3.getName(), ""));
        @NotNull final TaskDTO task = SERVICE.create(USER_ID, USER_TASK3.getName(), USER_TASK3.getDescription());
        Assert.assertNotNull(task);
        @Nullable final TaskDTO findTask = SERVICE.findOneById(USER_ID, task.getId());
        Assert.assertNotNull(findTask);
        Assert.assertEquals(task.getId(), findTask.getId());
        Assert.assertEquals(USER_TASK3.getName(), task.getName());
        Assert.assertEquals(USER_TASK3.getDescription(), task.getDescription());
        Assert.assertEquals(USER_ID, task.getUserId());
    }

    @Test
    public void existsByIdByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.existsById("", NON_EXISTING_TASK_ID));
        Assert.assertFalse(SERVICE.existsById(USER_ID, ""));
        Assert.assertFalse(SERVICE.existsById(USER_ID, NON_EXISTING_TASK_ID));
        Assert.assertTrue(SERVICE.existsById(USER_ID, USER_TASK1.getId()));
    }

    @Test
    public void findAllByProjectId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            @Nullable final Collection<TaskDTO> testCollection = SERVICE.findAllByProjectId(
                    null, USER_PROJECT1.getId());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            @Nullable final Collection<TaskDTO> testCollection = SERVICE.findAllByProjectId("", USER_PROJECT1.getId());
        });
        @NotNull final Collection<TaskDTO> emptyCollection = Collections.emptyList();
        Assert.assertEquals(emptyCollection, SERVICE.findAllByProjectId(USER_TEST.getId(), null));
        Assert.assertEquals(emptyCollection, SERVICE.findAllByProjectId(USER_TEST.getId(), ""));
        final List<TaskDTO> tasks = SERVICE.findAllByProjectId(USER_ID, USER_PROJECT1.getId());
        Assert.assertNotNull(tasks);
        Assert.assertEquals(2, tasks.size());
        tasks.forEach(task -> Assert.assertEquals(USER_ID, task.getUserId()));
        tasks.forEach(task -> Assert.assertEquals(USER_PROJECT1.getId(), task.getProjectId()));
    }

    @Test
    public void findAllByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.findAll(""));
        final List<TaskDTO> tasks = SERVICE.findAll(USER_ID);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(2, tasks.size());
        tasks.forEach(task -> Assert.assertEquals(USER_ID, task.getUserId()));
    }

    @Test
    public void findAllSortByUserId() throws Exception {
        @Nullable CustomSort sort = null;
        Assert.assertNotNull(SERVICE.findAll(USER_ID, sort));
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            @Nullable CustomSort sortInner = null;
            SERVICE.findAll("", sortInner);
        });
        sort = CustomSort.BY_NAME;
        final List<TaskDTO> tasks = SERVICE.findAll(USER_ID, sort);
        Assert.assertNotNull(tasks);
        tasks.forEach(task -> Assert.assertEquals(USER_ID, task.getUserId()));
    }

    @Test
    public void findOneByIdByUserId() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> SERVICE.findOneById(USER_ID, ""));
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.existsById("", USER_TASK1.getId()));
        Assert.assertNull(SERVICE.findOneById(USER_ID, NON_EXISTING_TASK_ID));
        @Nullable final TaskDTO task = SERVICE.findOneById(USER_ID, USER_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK1.getId(), task.getId());
    }

    @Test
    public void getSizeByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.getSize(""));
        Assert.assertEquals(2, SERVICE.getSize(USER_ID));
    }

    @Test
    public void removeByIdByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.removeOneById(null, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.removeOneById("", null));
        Assert.assertThrows(IdEmptyException.class, () -> SERVICE.removeOneById(USER_ID, null));
        Assert.assertThrows(IdEmptyException.class, () -> SERVICE.removeOneById(USER_ID, ""));
        Assert.assertThrows(EntityNotFoundException.class, () -> SERVICE.removeOneById(USER_ID, NON_EXISTING_TASK_ID));
        SERVICE.removeOneById(USER_ID, USER_TASK2.getId());
        Assert.assertNull(SERVICE.findOneById(USER_ID, USER_TASK2.getId()));
    }

    @Test
    public void removeByUserId() throws Exception {
        SERVICE.remove(USER_ID, USER_TASK2);
        Assert.assertNull(SERVICE.findOneById(USER_ID, USER_TASK2.getId()));
    }

    @Test
    public void updateById() throws Exception {
        Assert.assertThrows(
                UserIdEmptyException.class, () -> SERVICE.updateOneById(null, USER_TASK1.getId(), USER_TASK1.getName(),
                                                                        USER_TASK1.getDescription()
                ));
        Assert.assertThrows(
                UserIdEmptyException.class, () -> SERVICE.updateOneById("", USER_TASK1.getId(), USER_TASK1.getName(),
                                                                        USER_TASK1.getDescription()
                ));
        Assert.assertThrows(
                IdEmptyException.class, () -> SERVICE.updateOneById(USER_ID, null, USER_TASK1.getName(),
                                                                    USER_TASK1.getDescription()
                ));
        Assert.assertThrows(
                IdEmptyException.class,
                () -> SERVICE.updateOneById(USER_ID, "", USER_TASK1.getName(), USER_TASK1.getDescription())
        );
        Assert.assertThrows(
                NameEmptyException.class,
                () -> SERVICE.updateOneById(USER_ID, USER_TASK1.getId(), null, USER_TASK1.getDescription())
        );
        Assert.assertThrows(
                NameEmptyException.class,
                () -> SERVICE.updateOneById(USER_ID, USER_TASK1.getId(), "", USER_TASK1.getDescription())
        );
        Assert.assertThrows(
                DescriptionEmptyException.class,
                () -> SERVICE.updateOneById(USER_ID, USER_TASK1.getId(), USER_TASK1.getName(), null)
        );
        Assert.assertThrows(
                DescriptionEmptyException.class,
                () -> SERVICE.updateOneById(USER_ID, USER_TASK1.getId(), USER_TASK1.getName(), "")
        );
        Assert.assertThrows(
                TaskNotFoundException.class,
                () -> SERVICE.updateOneById(USER_ID, NON_EXISTING_TASK_ID, USER_TASK1.getName(),
                                            USER_TASK1.getDescription()
                )
        );
        @NotNull final String name = USER_TASK1.getName() + NON_EXISTING_TASK_ID;
        @NotNull final String description = USER_TASK1.getDescription() + NON_EXISTING_TASK_ID;
        SERVICE.updateOneById(USER_ID, USER_TASK1.getId(), name, description);
        @Nullable final TaskDTO task = SERVICE.findOneById(USER_ID, USER_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(name, task.getName());
        Assert.assertEquals(description, task.getDescription());
    }

}