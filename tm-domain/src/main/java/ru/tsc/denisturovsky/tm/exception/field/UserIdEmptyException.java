package ru.tsc.denisturovsky.tm.exception.field;

public class UserIdEmptyException extends AbstractFieldException {

    public UserIdEmptyException() {
        super("Error! User id is empty.");
    }

}
